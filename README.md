# Dromiciops
## The MeLi-ddelware

Connect to MeLi apis without oAuth
¡Look ma', I'm riding without tokens!

Go From this:

https://api.mercadolibre.com/users/me?access_token=*your_long_token_which_you_don't_know_how_to_get*

To this:

POST - localhost:4000/api - {endpoint: "/users/me", method: "GET", user: "**user_id**"} - Header: "passwd:**PASSWD**"

#### Requirements:
  * Redis

#### Start the app:

  * Install dependencies with `mix deps.get`
  * Install Node.js dependencies with `npm install`
  * Start Phoenix endpoint with `PASSWD=*PASSWD* mix phoenix.server` (use a custom password)

Visit [`localhost:4000`](http://localhost:4000) (user: user, password: **PASSWD**)

Login with MeLi (you can use many MeLi users)

Then, just use your local apis:

For POST :

POST - /api - {endpoint: "**resource**", method: "POST", user: "**user_id**", document: {**document**}} - Header: "passwd:**PASSWD**"

For GET :

POST - /api - {endpoint: "**resource**", method: "GET", user: "**user_id**", attrs: {**attr1: "attr1"**}} - Header: "passwd:**PASSWD**"

For MeLi API resources, check http://developers.mercadolibre.com/api-docs/

### Examples:

POST TEST USER:

    curl -XPOST -H "Content-Type:application/json" -H "passwd:PASSWD" -d '{"endpoint": "/users/test_user", "method": "POST", "user": "111111111", "document": {"site_id": "MLC"}}' localhost:4000/api

GET MY INFO:

    curl -XPOST -H "Content-Type:application/json" -H "passwd:PASSWD" -d '{"endpoint": "/users/me", "method": "GET", "user": "111111111"}' localhost:4000/api

GET MY ORDERS:

    curl -XPOST -H "Content-Type:application/json" -H "passwd:PASSWD" -d '{"endpoint": "/orders/search", "method": "GET", "user": "111111111", "attrs": {"seller": "111111111"}}' localhost:4000/api

GET MY SOLD ITEMS:

    curl -XPOST -H "Content-Type:application/json" -H "passwd:PASSWD" -d '{"endpoint": "/orders/search", "method": "GET", "user": "111111111", "attrs": {"seller": "111111111"}}' localhost:4000/api

POST NEW ITEM:

    curl -XPOST -H "Content-Type:application/json" -H "passwd:PASSWD" -d '{"endpoint": "/items", "method": "POST", "user": "111111111", "document": {"title": "Rata con leptospirosis, no comprar", "category_id": "MLC1109", "listing_type_id": "gold", "currency_id": "CLP", "price": 1000000, "available_quantity": 1, "description":"IGUAL ES LINDA", "condition": "used"}}' localhost:4000/api

UPDATE MY ITEM:

    curl -XPOST -H "Content-Type:application/json" -H "passwd:PASSWD" -d '{"endpoint": "/items/MLC439596453", "method": "PUT", "user": "111111111", "document": {"pictures": [{"source": "http://www.animalfactguide.com/wp-content/uploads/2013/08/quokka1.jpg"}]}}' localhost:4000/api

CLOSE MY ITEM:

    curl -XPOST -H "Content-Type:application/json" -H "passwd:PASSWD" -d '{"endpoint": "/items/MLC439596453", "method": "PUT", "user": "111111111", "document": {"status":"closed"}}' localhost:4000/api

Built with ♥️ by MelonCargo
in #700 DO
