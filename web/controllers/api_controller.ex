defmodule Dromiciops.ApiController do
  use Dromiciops.Web, :controller

  #Check passwd
  def call_api(%{req_headers: headers, body_params: body_params}=conn, _ ) do
    pass = passwd()
    case header_pass(headers) do
      {"passwd", ^pass} ->
        send_body(conn, body_params)
      _ ->
        send_resp(conn, 401, "Nope.")
    end
  end


  #POST
  def send_body(conn, %{"method" => "POST", "endpoint" => endpoint, "user" => user, "document" => doc}) do
    {code, response} = Api.post(user, endpoint, doc)
    conn
      |> put_status(code)
      |> json(response)
  end

  def send_body(conn, %{"method" => "PUT", "endpoint" => endpoint, "user" => user, "document" => doc}) do
    {code, response} = Api.put(user, endpoint, doc)
    conn
      |> put_status(code)
      |> json(response)
  end

  def send_body(conn, %{"method" => "GET", "endpoint" => endpoint, "user" => user, "attrs" => attrs}) do
    {code, response} = Api.get(user, endpoint, attrs)
    conn
      |> put_status(code)
      |> json(response)
  end

  def send_body(conn, %{"method" => "GET", "endpoint" => endpoint, "user" => user}) do
    {code, response} = Api.get(user, endpoint)
    conn
      |> put_status(code)
      |> json(response)
  end

  def send_body(conn, %{"method" => "DELETE", "endpoint" => endpoint, "user" => user}) do
    {code, response} = Api.delete(user, endpoint)
    conn
      |> put_status(code)
      |> json(response)
  end



  #ERROR
  def send_body(conn, _) do
    send_resp(conn, 422, "Bad format")
  end

  defp passwd do
    System.get_env("PASSWD")
  end

  defp header_pass(headers) do
    Enum.find(headers, fn
      {"passwd", _} -> true
      _ -> false
    end)
  end

end
