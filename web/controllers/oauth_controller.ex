defmodule Dromiciops.OauthController do
  use Dromiciops.Web, :controller

  def index(conn, _params) do
    users = Exredis.Api.keys(:redis, "*")
    render conn, "index.html", [
      users: users,
      code_url: Api.code_url,
      passwd: System.get_env("PASSWD"),
      user: Enum.at(users, -1)
    ]
  end

  def callback(conn, %{"code" => code}) do
    Api.set_access_token(code)
    redirect conn, to: "/"
  end

  def delete_user(conn, %{"user_id" => user_id}) do
   Exredis.Api.del(:redis, user_id)
   redirect conn, to: "/"
  end
end
