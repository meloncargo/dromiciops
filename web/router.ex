defmodule Dromiciops.Router do
  use Dromiciops.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug BasicAuth, username: "user", password: System.get_env("PASSWD")
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Dromiciops do
    pipe_through :browser # Use the default browser stack

    get "/", OauthController, :index
    get "/callback", OauthController, :callback
    get "/delete_user/:user_id", OauthController, :delete_user
  end

  # Other scopes may use custom stacks.
  scope "/api", Dromiciops do
    pipe_through :api

    post "/", ApiController, :call_api
  end
end
