defmodule Api do
  @api_root_url "https://api.mercadolibre.com"
  @auth_url "http://auth.mercadolibre.com/authorization"
  @oauth_url "/oauth/token"
  @host "http://localhost:4000"
  @client_id  "111121351911009" # (app_id)
  @secret "7yz1oXfXaKVykpTjSiQez2pNzmj8nnxU"
  @callback_url "http://localhost:4000/callback"

  def post(user, endpoint, doc) do
    access_token = Exredis.Api.hget(:redis, user, "access_token")
    url = "#{@api_root_url <> endpoint}?access_token=#{access_token}"
    document = doc |> Poison.encode!
    case HTTPoison.post(url, document) do
      {:ok, %HTTPoison.Response{status_code: 401}} ->
        refresh_token(user)
        post(user, endpoint, doc)
      {:ok, %HTTPoison.Response{status_code: code, body: body} } ->
        {code, Poison.decode!(body)}
      {:error, %HTTPoison.Error{reason: reason} } ->
        {500, %{error: reason}}
      _ ->
        {500, %{error: "I don't know what i'm doing"}}
    end
  end

  def put(user, endpoint, doc) do
    access_token = Exredis.Api.hget(:redis, user, "access_token")
    url = "#{@api_root_url <> endpoint}?access_token=#{access_token}"
    document = doc |> Poison.encode!
    case HTTPoison.put(url, document) do
      {:ok, %HTTPoison.Response{status_code: 401}} ->
        refresh_token(user)
        put(user, endpoint, doc)
      {:ok, %HTTPoison.Response{status_code: code, body: body} } ->
        {code, Poison.decode!(body)}
      {:error, %HTTPoison.Error{reason: reason} } ->
        {500, %{error: reason}}
      _ ->
        {500, %{error: "I don't know what i'm doing"}}
    end
  end

  def get(user, endpoint, params \\ %{}) do

    access_token = Exredis.Api.hget(:redis, user, "access_token")
    parameters = URI.encode_query(Map.merge(params, %{access_token: access_token}))
    url = @api_root_url <> endpoint <> "?" <> parameters
    case HTTPoison.get(url) do
      {:ok, %HTTPoison.Response{status_code: 401}} ->
        refresh_token(user)
        get(user, endpoint, params)
      {:ok, %HTTPoison.Response{status_code: code, body: body} } ->
        {code, Poison.decode!(body)}
      {:error, %HTTPoison.Error{reason: reason} } ->
        {500, %{error: reason}}
      _ ->
        {500, %{error: "I don't know what i'm doing"}}
    end
  end

  def delete(user, endpoint) do
    access_token = Exredis.Api.hget(:redis, user, "access_token")
    url = @api_root_url <> endpoint <> "?" <> "access_token=#{access_token}"
    case HTTPoison.delete(url) do
      {:ok, %HTTPoison.Response{status_code: 401}} ->
        refresh_token(user)
        delete(user, endpoint)
      {:ok, %HTTPoison.Response{status_code: code, body: body} } ->
        {code, Poison.decode!(body)}
      {:error, %HTTPoison.Error{reason: reason} } ->
        {500, %{error: reason}}
      _ ->
        {500, %{error: "I don't know what i'm doing"}}
    end
  end

  def code_url do
    "#{@auth_url}?response_type=code&client_id=#{@client_id}&redirect_uri=#{@callback_url}"
  end

  def set_access_token(code) do
    params = %{
      grant_type: "authorization_code",
      client_id: @client_id,
      client_secret: @secret,
      code: code,
      redirect_uri: @callback_url
    } |> Poison.encode!
    url = @api_root_url <> @oauth_url

    case HTTPoison.post(url, params) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body } } ->
        resp = Poison.decode!(body, keys: :atoms)
        Exredis.Api.hmset(:redis, resp.user_id, ["access_token", resp.access_token,
                                                 "refresh_token", resp.refresh_token])
        set_nickname(resp.user_id)
      {:ok, %HTTPoison.Response{status_code: code } } ->
        {:error, code}
      {:error, %HTTPoison.Error{reason: reason}} ->
        {:error, reason}
      whatever -> whatever
    end
  end

  def refresh_token(user_id) do
    params = %{
      grant_type: "refresh_token",
      refresh_token: Exredis.Api.hget(:redis, user_id, "refresh_token"),
      client_id: @client_id,
      client_secret: @secret
    } |> Poison.encode!

    url = @api_root_url <> @oauth_url

    case HTTPoison.post(url, params) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body } } ->
        resp = Poison.decode!(body, keys: :atoms)
        Exredis.Api.hmset(:redis, resp.user_id, ["access_token", resp.access_token,
                                                 "refresh_token", resp.refresh_token])
      {:ok, %HTTPoison.Response{status_code: code, body: body } } ->
        {:error, code}
      {:error, %HTTPoison.Error{reason: reason}} ->
        {:error, reason}
      whatever -> whatever
    end
  end

  def set_nickname(user) do
    {200, %{"nickname" => nickname}} = get(user, "/users/me")
    Exredis.Api.hset(:redis, user, "nickname", nickname)
  end


  #
  # class AccessError < StandardError; end;
  #
  # attr_accessor :user, :user_id
  # def initialize(options={})
  #   HTTParty::Basement.default_options.update(verify: false) unless Rails.env.production?
  #   self.user = options[:user] #user is a key to identify the meli user, example :meloncargo
  #
  #   @config = {
  #     api_root_url: 'https://api.mercadolibre.com',
  #     auth_url: 'http://auth.mercadolibre.com/authorization',
  #     oauth_url: '/oauth/token'
  #   }
  #
  #   callback_path = "/api/melis/callback/#{user}"
  #   if Rails.env.production?
  #     @host = DEFAULT_HOST
  #     @client_id = '1118635087041495' # (app_id)
  #     @secret = 'dVWStHtdGxkt1yWrA1HUll2FeJUhu2te'
  #     @callback_url = @host + callback_path
  #   else
  #     #use the quokka_dev connection
  #     @host = "http://localhost:3000"
  #     @client_id = '3908281585356731' # (app_id)
  #     @secret = 'pUV1PBSeoqS0SxtQprgR70HrcAHvqIQk'
  #     @callback_url = @host + callback_path
  #   end
  #   @site = options[:site] || "MLC"
  #
  #   self.user_id = $redis.get("#{user}:meli_id")
  #
  #   if options[:code].present?
  #     set_access_token(options[:code])
  #   end
  # end
  #
  # def my_info
  #   response = get("/users/me")
  #   $redis.set("#{user}:meli_id", response["id"])
  #   response
  # end
  #
  # def get_seller_items(seller, paging={})
  #   paging[:limit] ||= 50
  #   paging[:offset] ||= 0
  #   url = "/sites/#{@site}/search?" + to_url({seller_id: seller, limit: paging[:limit], offset: paging[:offset]})
  #   get(url)
  # end
  #
  # def get_my_items(statuses=["active"], paging={})
  #   paging[:limit] ||= 100
  #   paging[:offset] ||= 0
  #   url = "/users/#{self.user_id}/items/search?limit=#{paging[:limit]}&offset=#{paging[:offset]}&status=#{statuses*','}"
  #   get(url)
  # end
  #
  # def get_all_seller_items(seller=self.user_id)
  #   limit = 200
  #   offset = 200
  #   response = get_seller_items(seller, {limit: limit})
  #   @items = response["results"]
  #   ((response["paging"]["total"].to_f/limit.to_f).ceil-1).times do
  #     @items += get_seller_items(seller, {offset: offset, limit: limit})["results"]
  #     offset += limit
  #   end
  #   @items
  # end
  #
  # # Gets item info by id ex: get_item("MLC429279368", ["category_id"])
  # def get_item(item_id, attributes=[])
  #   url = "/items/#{item_id}"
  #   url += "?attributes=#{attributes*','}" if attributes.present?
  #   get(url)
  # end
  #
  # def get_description(item_id)
  #   get("/items/#{item_id}/description")
  # end
  #
  # # Updates item info by id ex: get_item("MLC429279368", {price: 31232}), if description is true, sets description object
  # def update_item(item_id, document, description=false)
  #   url = description ? "/items/#{item_id}/description" : "/items/#{item_id}"
  #   put(url, document)
  # end
  #
  # def close_item(item_id)
  #   update_item(item_id, {status: "closed"} )
  # end
  #
  # def publish_item(document)
  #   post("/items", document)
  # end
  #
  # # Gets order info by id
  # def get_order(order_id)
  #   get("/orders/#{order_id}")
  # end
  #
  # def category(category_id, force=false)
  #   Rails.cache.fetch(['category', category_id], expires: 1.day, force: force) do
  #     HTTParty.get(@config[:api_root_url]+"/categories/#{category_id}").parsed_response
  #   end
  # end
  #
  # def shipping(category_id, force=false)
  #   Rails.cache.fetch(['shipping', category_id], expires: 1.day, force: force) do
  #     HTTParty.get(@config[:api_root_url]+"/categories/#{category_id}/shipping").parsed_response
  #   end
  # end
  #
  # def get(query)
  #   begin
  #     request = HTTParty.get url(query)
  #     raise AccessError, request.code if request.code == 401
  #     request.parsed_response
  #   rescue AccessError => e
  #     set_refresh_token
  #     retry
  #   end
  # end
  #
  # def post(query, document)
  #   begin
  #     request = HTTParty.post url(query), body: document.to_json, headers: { 'Content-Type' => 'application/json' }
  #     raise AccessError, request.code if request.code == 401
  #     request.parsed_response
  #   rescue AccessError => e
  #     set_refresh_token
  #     retry
  #   end
  # end
  #
  # def put(query, document)
  #   begin
  #     request = HTTParty.put url(query), body: document.to_json
  #     raise AccessError, request.code if request.code == 401
  #     request.parsed_response
  #   rescue AccessError => e
  #     set_refresh_token
  #     retry
  #   end
  # end
  #
  # def expire_me
  #   $redis.del("#{user}*")
  #   HTTParty.delete url("/users/#{my_info['id']}/applications/#{@client_id}")
  # end
  #
  # def get_code_url
  #   params = {response_type: "code", client_id: @client_id, redirect_uri: @callback_url}
  #   return @config[:auth_url] + '?' + to_url(params)
  # end
  #
  # def set_access_token(code)
  #
  #   raise "We don't have any code." if code.blank?
  #
  #   params = {
  #     grant_type: "authorization_code",
  #     client_id: @client_id,
  #     client_secret: @secret,
  #     code: code,
  #     redirect_uri: @callback_url
  #   }
  #   url = @config[:api_root_url] + @config[:oauth_url]
  #   request = HTTParty.post(url, body: params)
  #   response = request.parsed_response
  #   raise response["message"] if request.response.code != "200"
  #
  #   $redis.set("#{user}:access_token", response["access_token"])
  #   $redis.set("#{user}:refresh_token", response["refresh_token"])
  #   #gets my info to set meli:current_id
  #   my_info
  #
  # end
  #
  # def set_refresh_token
  #   params = {
  #     grant_type: "refresh_token",
  #     refresh_token: $redis.get("#{user}:refresh_token"),
  #     client_id: @client_id,
  #     client_secret: @secret
  #   }
  #   request = HTTParty.post(@config[:api_root_url] + @config[:oauth_url], body: params )
  #   response = request.parsed_response
  #   raise response["message"] if request.code != 200
  #   $redis.set("#{user}:access_token", response["access_token"])
  #   $redis.set("#{user}:refresh_token", response["refresh_token"])
  #
  # end
  #
  # def access_token
  #   $redis.get("#{user}:access_token")
  # end
  #
  # def test_user
  #   post("/users/test_user", {site_id: @site} )
  # end
  #
  # def to_url(params)
  #   URI.escape(params.collect{|k,v| "#{k}=#{v}"}.join('&')) if params
  # end
  #
  # def url(route)
  #   sep = '?'.in?(route) ? '&' : '?'
  #   @config[:api_root_url] + route + sep + 'access_token=' + access_token.to_s
  # end
  #
end
